<?php

return [
    'KLIENT_NUMBER_TEST' => '',
    'KLIENT_KEY_TEST'    => '',
    'KLIENT_CURRENCY'    => 'RUB',
    'IS_TEST'            => true,

    'DB' => [
        'DSN'      => 'mysql:host=localhost;dbname=DB_NAME',
        'USERNAME' => '',
        'PASSWORD' => '',
        'DRIVER'   => null,
        'PDO'      => null,
    ],
];